# 2本目の作品 (2019/11)
## タイトル：【30代女性にオススメ】Webライターの始め方3ステップ

ここ数年、多様な働き方ができる社会を目指して作られた「働き方改革」が注目を集めています。

あなたもこんな経験をしたことは無いですか？  
・今日は彼氏とのデートだったけど、急な仕事でキャンセルになってしまった  
・毎日残業で、友人との食事の約束は遅刻してばっかり

今、そんな悩みを抱える人たちから、Webライターという仕事が注目を集めています。

Webライターは、パソコンやタブレットが1台あれば、お気に入りのカフェや移動中の待ち時間でも仕事が出来るため、時間や場所に縛られない、自由な働き方が出来るお仕事です。  
あなたも、彼氏や友人とのプライベートの時間を自由に持つことができ、周りの人から羨ましがられるような仕事につけたら良いなと思いませんか。

でも、「Web業界の仕事なんてしたことが無いし、30代になって新しい仕事を始められるのか不安だよ」という人も多いと思います。

この記事では、そんなあなたのために、Webライターという仕事に必要な資格やスキルをお伝えし、実際にWebライターを始めるための方法を紹介していきます。

### Webライターの仕事内容とその魅力

Webライターは、インターネット上に掲載されている、文章を書くお仕事です。

ファッション情報サイトのおすすめ商品の紹介や、ショッピングサイトのレポート記事など、身近な所にもWebライターの書いた記事があふれています。

以前は、それぞれのサイトを運営している会社に所属し、そのサイトの専属ライターとしての働き方が主流でした。  
しかし、最近では、フリーランスのWebライターという働き方も多くなってきています。

ここ数年は、クラウドソーシングと呼ばれる、インターネット上で仕事の依頼から支払いまでのやり取りが完結するサービスが充実してきています。  
このため、企業は1記事からでも執筆の依頼ができますし、Webライターもその依頼を請け負って記事を書くことができます。  
さらに、インターネット上ですべてを完結することもできるので、お気に入りのカフェや、旅行先のホテルのラウンジなど、どんな場所も仕事場にできてしまいます。

また、依頼主との連絡以外の時間は、基本的に自由に使う事も可能なため、学生や主婦の方などが、すきま時間を活用して働くケースも増えています。

### 30代の転職にWebライターを始めることをお勧めする理由

ここでは、なぜ30代の転職にWebライターという職業をオススメするかを紹介します。

#### 採用のハードルが低い

Webライターは、文章を書くための基本的な語彙力があれば、誰でも挑戦するチャンスがあり、決して難易度は高くありません。  
また、未経験であれば未経験ならではの視点でしか書けない文章がありますし、30代であれば30代にしか書けない文章もあります。

自分にしか書けない記事であるとアピールすることができれば、年齢や境遇を一つの強みにして、採用活動に臨むことができることも採用のハードルが低いとされている理由です。

#### 年齢に関係なく評価される

30代を迎えると、「今から転職なんてできるのかな？」と不安になるため、特に未経験の仕事に進むのは勇気が要ります。  
また、採用する企業側からも、年齢に応じた資格やスキルが求められて来ます。

しかし、Webライターの場合の仕事は、学生や主婦そして高齢者なども努めており、年齢制限はありません。さらに、クラウドソーシングのように相手が見えない場合も多いため、一般の転職活動のように「年齢の割には」という評価をされることはありません。

#### プライベートを大事に出来る

冒頭でもお伝えした通り、Webライターの仕事は、「インターネット上に掲載されている、文章を書くこと」です。  
このため、依頼主との連絡さえキチンと取れていれば、それ以外の時間は基本的に自由に働く場所を選ぶことができます。

旅行中のホテルのラウンジや、カフェそして自宅でも仕事ができるため、プライベートと両立がしやすく、主婦や学生などでもWebライターとして活躍している人がたくさんいます。

### Webライターの働き方と収入

Webライターの収入は、副業でお小遣い稼ぎをしている人から、正社員で働く人まで、働き方によって大きく変わってきます。

#### 副業

主婦や学生が家事や勉強の合間の時間を有効活用したり、会社員の人が現在の収入プラスαを得るために、平日の夜や週末を利用したりして、少しだけ稼ぐ方法です。

副業でできるWebライターの収入は、[案件単価]または[文字単価]で決まります。

[案件単価]  
1記事のみを作成することもあれば、20記事で1案件のように、まとまった記事数を1案件として扱い、1案件あたりの単価が設定されています。

[文字単価]  
1文字あたり1円のように、書いた文字数に1文字当たりの単価を掛けた金額で収入が決まります。

どちらの場合も、依頼を出す人(発注者)から単価を指定されます。  
自分の経験やスキルによって、受けられる依頼が変わってきますし、継続して案件を受注することができれば、単価アップの交渉もしやすくなります。  
つまり、スキルアップ次第で収入アップも夢ではありません。

#### 転職(正社員・契約社員など)

今の仕事を辞めて、Webライターを本業にしたい場合は、特定の企業に転職して、その会社の専属ライターとして所属します。

求人・転職サイトの「はたらいく」の調査で、ライターの平均年収は 269万円と言われています。  
参照：[ライター・記者・編集者の年収・月収データ](https://www.hatalike.jp/h/r/H1ZZ280s.jsp?hp=%2Fresearch%2Fn0088%2Findex.html&noRedF=1&__u=1572702582107-7790728192118188489)

「思ったより低い」と思った人も多いのではないでしょうか。  
はい。Webライターは特別に高収入というわけではありません。

しかし、それはライターのスキルによって大きく変わります。  
ライターとして社内で評価されれば、収入はアップしていきます。

しかも、専属ライターの場合は、マーケティングやSEOという専門分野にもかかわっていくことが多く、ライティング以外のスキルも向上することで収入アップを目指すこともできます。

#### フリーランス

会社や組織に縛られず、自分のスキルだけで自由に働き方や仕事相手を選んで働ける働き方です。「将来はフリーランスになりたい！」という人も多いかもしれませんね。

フリーランスの収入は、仕事が取れるかどうかで決まってきます。  
最悪の場合は、0円という事もあるかもしれません。

しかし、仕事を取る事さえできれば、頑張りしだいで収入の大幅アップも出来るのがフリーランスです。実際に、月収が〇百万円とか、年収〇千万円というWebライターもたくさんいます。  
大変だけれども、すごく夢のある働き方ですね。

### Webライターに必要な資格とスキル

次に、Webライターに必要な資格とスキルについてお伝えしていきます。

#### Webライターに必要な資格

ここまで読みすすめてくれたあなたは、もう気づいているかもしれませんが、Webライターには特に資格は必要ありません。

Webライター向けの資格もいくつか出てきてはいますが、資格がないとWebライターになれないなんてことはありませんので、安心してください。
もちろん、持っていれば転職や依頼を受けるために有利になることもあると思いますので、持っていて損は無いと思います。

#### Webライターに必要なスキル

Webライターに必要なスキルは、「記事を読む人にわかりやすく、正確に伝えられること」です。  
記事の中で伝えるべきことを、文章を使ってわかりやすく、正確に表現するための文章力が必要になります。

また、記事を読む人が「何を知りたがっているか」を読み取って、文章の中に盛り込むスキルが必要になります。  
相手が「何を知りたがっているか」を理解することが、わかりやすい文章を書くためのはじめの一歩です。

そのためには、記事を読む対象者の年齢・性別・性格などを設定し、その人に届けるつもりになって、どんな内容の文章にしていくかを絞り込んでいきます。  
そうすることで、「記事を読む人は何に興味があって、どんな疑問が湧いてくるかを想像し、記事を読むことで何を伝えることができるか」、という事が具体的にわかるようになります。

### Webライターの始め方3ステップ

では、Webライターを始めるためには、具体的に何をしたらよいかお伝えしていきます。

Web業界未経験の状態から、いきなり転職やフリーランスになろうとするのは、かなりハードルが高いかもしれません。  
企業は経験者を求めていることが多いですし、フリーランスに依頼する場合も、「未経験者はNG」なんてことが良くあります。

まずはクラウドソーシングを使って、未経験者OKな仕事を受注することで、Webライターとしてのスキルを磨き、経験を積んでいくことをオススメします。

#### ステップ1：文章の書き方を学習する

日頃から文章を書いている人でなければ、いきなり仕事を受注するのには抵抗があると思います。そのような場合は、文章の書き方を学習しましょう。

Webライターの文章の書き方を学ぶ方法はたくさんありますのでどれを選んでも構いません。多くの人は、専門のスクールに通ったり、参考書から独学で学んだりしています。

筆者の場合は、Webライターの専門スクールに通って学習しました。  
Webライターを始めるためのノウハウが詰まっていますので、オススメの方法です。

#### ステップ2：クラウドソーシングに登録する

本記事では既に何度も登場している、クラウドソーシングに登録しましょう。

ここでは、オススメのクラウドソーシングサービスを4つ紹介します。  
最初は幾つかやってみるのも良いと思いますが、複数サービスでまとめて仕事をしていたりすると、依頼主との連絡が煩雑になるなど、効率よく経験を積むことができないため、最終的には自分にとって最も使いやすいサービス1つもしくは2つ程度に絞ると良いでしょう。

* クラウドワークス (https://crowdworks.jp/)
* ランサーズ (https://www.lancers.jp/)
* サグーワークス　(https://works.sagooo.com/)
* Bizseek (https://www.bizseek.jp/)

#### ステップ3：案件を受注する

クラウドソーシングサービスに登録したら、実際に案件を受注します。  
まずは未経験者OKの案件から始めてみて、経験を積んでみてください。

依頼者によっては、文章の書き方のポイントを教えてくれるような人や、書いた記事を細かく添削して、ライティング力の向上に協力してくれる人もいますので、まずは相談から始めてみましょう。

##### 継続案件を受注する

Webライターの仕事を取り始めることができたら、継続案件を受注できるように頑張ってみてください。  
継続案件とは、その名の通り継続して受注する依頼の事です。  
そのためには、依頼者に作成した記事を気に入ってもらう事はもちろん、「また次回もお願いしたい！」と思ってもらえるよう、お互いに気持ちの良いコミュニケーションを心がけましょう。

以上が、Webライターを始めるための3ステップです。

### 信頼できるWebライターの2つの特徴

ここまでで、実際にWebライターを始めるために必要なことを説明してきました。  
しかし、いざ仕事を受けようとすると、不安も大きいと思います。

ここでは、Webライターとして成長して、成功していくために、「信頼できるWebライター」の2つの特徴をお伝えします。

1. 問い合わせへの返信が早く、丁寧に対応する。  
   相手を待たせない、気持ち良い対応ができると、依頼者も安心して仕事を任せられますよね。
1. 早めに納品を心がけ、納期に遅れない。  
   期限ギリギリで出されるよりは、余裕を持って提出してくれる人の方が好印象です。

いかがでしょうか？「当然のことじゃないか。」と思った人がほとんどだと思います。  
しかし、当然のことを守ることは、依頼主からの信頼を勝ち取るためには最も重要な事なのです。

### まとめ

Webライターを始める方法はイメージ出来ましたか？  
「そんなに簡単に出来るわけないよ。」と思っている人も多いと思います。  
しかし、Webライターという職業が、特別な資格が不要で、初心者にも始めやすいという事はご理解いただけたのではないでしょうか。

本記事では、Webライターという仕事についてお伝えし、Webライターが30代女性の転職にピッタリであること、そして未経験者でも始めやすい仕事であることを明らかにした後に、実際にWebライターを始めるための3ステップを紹介しました。

あなたもWebライターを始めてみて、大切なパートナーとの旅行やデートの時間を自由に取ることのできる、理想の働き方を手に入れてみませんか。

## 基本事項

### ペルソナ

```
1. 名前
山下 美奈子

2. 性別・年齢
女性・33歳

3. 居住
神奈川県川崎市在住

4. 家族構成 (パートナーや子供の有無)
現在一人暮らし。
現在の彼氏との結婚を考えて同棲の相談中。

5. 仕事 (業種 / 役職 / 経歴 / 収入)
5年間のアパレルのショップ店員から近隣の店舗を束ねるマネージャに昇進し3年目になった。
年収 450万円

6. 趣味 (仕事後 / 休日の過ごし方)
仕事が忙しく、終わると近所のデパートでお弁当を購入して帰宅し、テレビを見ながらゆっくりするのが日課。
休日はランチやショッピング・映画など彼氏とのデートを楽しんでいる。

7. 好きな本や雑誌、メディア
洋服が好きで「Stedy.」を定期購読している。近年は家庭的な女性を意識して「VERY」をよく見る

8. 情報収集の方法 (テレビ / SNS / メディアサイト)
朝はめざましテレビを見るのが日課。通勤中は「elle.com」等を利用しいてファッションの流行についてキャッチアップしている。
よく見るWebサイトは「Oggi.jp」。

9. 性格・価値観
向上心が強く、仕事でもプライベートでも常に成長を心がけている。
常に明るく誰とでも平等に接する性格で、周りの面倒見もよく、頼れるお姉さんのような存在になっている。
仕事の後輩からは憧れられる一方、何でも出来なければというプレッシャーが強くなってきて息苦しさも感じている。

10. 現状の悩み
仕事に全力で取り組んできて、やりがいもあり、職場内では生え抜きで順調に昇進しているため、自信は持っている反面、自分の時間が取れていないことに不満を持っている。
彼氏との結婚を意識するようになり、家庭との両立を意識してから、プライベートな時間を大切にしたいと思うようになってきた。
転職を考えたいものの、自社での業務しか知らないため、他の仕事ができるのか不安を抱えている。
```

### テーマ

```
ジャンル: Webライター
テーマ: 始め方
```
